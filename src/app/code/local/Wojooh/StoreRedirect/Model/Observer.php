<?php

class Wojooh_StoreRedirect_Model_Observer {

    /**
     * Set store redirect
     *
     * @param Varien_Event_Observer $observer
     * @author Vadims Petrovs <vadims.petrovs@redboxdigital.com>
     */
    public function defaultStoreRedirect($observer)
    {
        Mage::log(Mage::app()->getStore()->getCode(), null, 'test', true);
        $storeCookie = Mage::getModel('core/cookie')->get('store');
        $storeCode = Mage::app()->getStore()->getCode();

        if (!$storeCookie && $storeCode == 'nocom_en') {
            $urlString = Mage::helper('core/url')->getCurrentUrl();
            $url = Mage::getSingleton('core/url')->parseUrl($urlString);
            $path = $url->getPath();
            $redirectPath = Mage::getBaseUrl() . 'ae-en' . $path . '?___store=uae_en';
            Mage::getModel('core/cookie')->set('store','uae_en', 31556926);
            Mage::getModel('core/cookie')->set(Mage_Core_Model_Store::COOKIE_NAME, 'uae_en', true);
            Mage::run('uae_en', 'store');
            Mage::app()->getFrontController()->getResponse()->setRedirect($redirectPath);
        }
    }



}
